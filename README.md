## The framework ##:-
![React native fw.png](https://bitbucket.org/repo/nqGLoG/images/2563280938-React%20native%20fw.png)

## Page Object driven ##:-
![image2016-12-7 10-39-2.png](https://bitbucket.org/repo/nqGLoG/images/2138908746-image2016-12-7%2010-39-2.png)

# Setting up and requirements, # 

This example will launch and test a React Native application from a MacOs/Windows

## Settings ##
Install the Android Studio using this guide React Native - Android
The player demo APK (Attached) has to be ran on an Android device ( the player cannot be run in a simulator )
 
Once studio has been installed create a react native project in the location of your choice with the init command
react-native init test1

( As this apk is already built we really don't need the react-native project but its a useful to layout this way if tests are performed on a working project, but for this you can just create a node project if need be )

From node package manager install global (npm i -g) the following packages.
appium
appium-doctor

Also install the appium inspector, from http://appium.io/ this is a standalone app that can be used to run the apps and interact with them ( get xpaths, record etc). Currently this doesn't work with the new Appium 1.6 (but may do by now)

From node package manager install local the following packages.
chai
chai-as-promised
cucumber
cucumber-html-report
wd
zombie

Now to check the install of Android Studio ( and Xcode ) in the console run
appium-doctor

You should get a report of the state of the install and if issues are found it will recommend actions to take like below.
![appium-doctor.png](https://bitbucket.org/repo/nqGLoG/images/3583118830-appium-doctor.png)

Issues to watch for are HOME / ANDROID_HOME / JAVA_HOME paths not set ( for macOS create a bash profile and for windows set the environment variables ).
Start up Terminal
Type "cd ~/" to go to your home folder
Type "touch .bash_profile" to create your new file.
Here's what mine looks like,  you can just change the paths.

export JAVA_HOME=$(/usr/libexec/java_home)
export ANDROID_HOME=/Users/tigger/Library/Android/sdk
[[ -s "$HOME/.profile" ]] && source "$HOME/.profile"
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"

Then type the code block to reload the bash profile
. .bash_profile

Create a folder in your documents folder and copy this app-release.apk to it. Also unzip this bundle to the project you created above testPlayer.zip
 
We will need to change a few files to reflect your environment, we need to get the android device ID ( just have one device connected) this device must be unlocked and usb debug enabled, if you get a lock screen error then change the usb cable options from usb charge only to file transfer.
in the console type the following...

adb devices

you will get a list of devices back ( for example 077b36b3 ), also to get the android version of the device in the console type.

adb shell getprop ro.build.version.release

( for example output 6.0.1)
Search and replace the files unzip for YOUR_DEVICE_ID and replace with your id from adb devices, the search and replace platformVersion: "6.0.1" and put your android version here. Search for FULL_PATH_TO_APP and replace with your full path to the APKabove.
Open a console window and start the appium server ( just type appium) 
cd in your project folder and to run the tests with output being logged to the console, type

cucumber.js
in windows its 'cucumber-js' to run the feature file
windows: also needed to change appium.js host to  :
var browser = wd.promiseChainRemote("127.0.0.1", 4723);

Hopefully at this point you will see the app being installed on the device and the following out in the console as the tests run.

![appiumTestConsole.png](https://bitbucket.org/repo/nqGLoG/images/3014078473-appiumTestConsole.png)

![image2016-12-7 10-45-42.png](https://bitbucket.org/repo/nqGLoG/images/2438357399-image2016-12-7%2010-45-42.png)

![image2016-12-7 10-45-48.png](https://bitbucket.org/repo/nqGLoG/images/278410216-image2016-12-7%2010-45-48.png)

Also if you want to capture the output and format it to html start the tests like this

cucumber.js --format json > report.json

And once its finished type

node createHtmlReport.js

example of formatted output.

![reporthtml.png](https://bitbucket.org/repo/nqGLoG/images/2136784605-reporthtml.png)

Note:-  Xpath changes when using the Nexus tablet:
var xPathToText = "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.widget.TextView[1]";
this.Given(/^I tap the play button$/, function (callback) {
    driver.elementByXPath("//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.view.View[1]/android.view.View[1]").click().then(function () {
        callback();
 });
});