var Report = require('cucumber-html-report');

var options = {
    source:    'report.json', // source json
    dest:      './htmlReports',          // target directory (will create if not exists)
    name:      'report.html',        // report file name (will be index.html if not exists
    title:     'Cucumber Report',    // Title for default template. (default is Cucumber Report)
    component: 'test'       // Subtitle for default template. (default is empty)
};

var report = new Report(options);
report.createReport();