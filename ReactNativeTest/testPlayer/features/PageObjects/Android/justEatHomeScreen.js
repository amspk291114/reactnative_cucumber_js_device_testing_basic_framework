var JustEatHomeScreenObjects = function(){
   this.input_postcode_textfield = {
        type: "resource-id",
        value:"com.justeat.app.uk:id/input_postcode"
    };

    this.find_restaurant_button = {
    	type: "resource-id",
    	value:"com.justeat.app.uk:id/button_search"
    };

    this.just_eat_text = {
    	type: "class",
    	value:"android.widget.TextView"
    };
};

exports.justEathomeScreen = JustEatHomeScreenObjects;