@appium
Feature: Just Eat App
As a dev I want automate testing Just Eat React native apps

  Scenario: As a user I should be able view the default page of just eat
    Given I wait for the app to load
    And Just Eat default page is displayed
    When I enter my postcode as "NP44 3FD"
      And I click on Find Restaurants