'use strict';

var driver = require('../support/appium.js').getDriver();
var xPathToText = "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.TextView[1]";
var xPathToButton = "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]";

module.exports = function() {

    this.setDefaultTimeout(60000);

    // extra time need so js bundle gets init
    this.Given(/^I wait for the app to load$/, function(callback) {
        setTimeout(function() {
            callback();
        }, 15000);
    });

    this.Given(/^I tap the play button$/, function(callback) {
        driver.elementByXPath("//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]").click().then(function() {
            callback();
        });
    });

    this.Then(/^The duration label should be set after loading$/, function(callback) {
        // wait for stream to load and check duration
        setTimeout(() => {
            driver.elementByXPath(xPathToText).text().then(function(text) {
                if (text === " Duration has been set ") {
                    callback();
                } else {
                    callback(new Error(text));
                }
            });
        }, 15000);
    });

};