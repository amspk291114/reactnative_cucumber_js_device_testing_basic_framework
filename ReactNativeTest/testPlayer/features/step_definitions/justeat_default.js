'use strict';
var appium = require('../support/appium.js');
var driver = appium.getDriver();
var platformName1 = appium.getPlatform();
var pageObjectsPath = platformName1 === "Android" ? "../PageObjects/Android/" : "../PageObjects/iOS/";
var JustEatHomeScreen = require(pageObjectsPath + "justEatHomeScreen.js").justEathomeScreen;
var home = new JustEatHomeScreen();

// var xPathToText = "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.TextView[1]";
// var xPathToButton = "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]";

module.exports = function() {

    this.setDefaultTimeout(6000);

    this.Given(/^Just Eat default page is displayed$/, function(callback) {
        //console.log (driver.elementByClass(home.just_eat_text.value).text());
        setTimeout(() => {
            driver.elementByClassName(home.just_eat_text.value).text().then(function(text) {
                if (text === "Find your flavour") {
                    callback();
                } else {
                    callback(new Error(text));
                }
            });
        }, 15000);
    });

    this.Given(/^I enter my postcode as "(.*?)"$/, function(postcode) {
        driver.elementById(home.input_postcode_textfield.value).sendKeys(postcode).then(function() {
            callback();
        });
    });


    this.When(/^I click on Find Restaurants$/, function(callback) {
        driver.elementById(home.find_restaurant_button.value).click().then(function() {
            callback();
        });
    });

};