var driver = require('../support/appium.js').getDriver();
var fs = require('fs');
var path = require('path');

var afterHooks = function () {

    this.setDefaultTimeout(60000);

    this.After(function(scenario,callback) {

        if(scenario.isFailed()) {

            scenario.attach('CleanUp...');
            
            var pathToStore = path.join( "myScreenShot1" + ".png");
            driver.takeScreenshot().then(function(data){

                var base64Data = data.replace(/^data:image\/png;base64,/,"");
                
                fs.writeFile(pathToStore, base64Data, 'base64', function(err) {
                    setTimeout(function(){

                        if(err){
                            scenario.attach(err);
                        }else{
                            callback('error');
                        }

                    },10000);

                });
            });

        }else{
            callback();
        }
    });
};

module.exports = afterHooks;
