"use strict";

var wd = require("wd");
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");

chai.use(chaiAsPromised);
chai.should();
chaiAsPromised.transferPromiseness = wd.transferPromiseness;

var desired = {
  "appium-version": "1.0",
  platformName: "Android",
  platformVersion: "7.0",
  deviceName: "00d5860b5179ab25",
  "app": "C:/Users/bendre/Perforce/dev_projects/client/project/joinin-ui/projects/test/ReactNativeTest/Just Eat_v4.1.0.33664.apk",
  "app-package": "com.test1",
  "app-activity": ""
};

var browser = wd.promiseChainRemote("127.0.0.1", 4723);

browser.init(desired).then(function() {
     return browser;
}).done();

var getDriver = function() {
  return browser;
};

var getPlatform =function() {
	return desired.platformName;
};

module.exports.getDriver = getDriver;
module.exports.getPlatform = getPlatform;
