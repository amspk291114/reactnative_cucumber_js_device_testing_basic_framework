var World, chai, chaiAsPromised;

chai             = require('chai');
chai_as_promised = require('chai-as-promised');
var fs = require('fs');

World = function World (callback) {

    var screenshotPath = "screenshots";

    if(!fs.existsSync(screenshotPath)) {
        console.info('Creating screenShot folder');
        fs.mkdirSync(screenshotPath);
    }

    chai.use(chai_as_promised);

    this.expect = chai.expect;

    callback();
};

module.exports.World = World;